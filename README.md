---
typora-copy-images-to: capture
typora-root-url: .
---
# OBS-Danmu

## 简介

基于
[blivedm](https://github.com/xfgryujk/blivedm.git)
而开发的面向OBS的插件，用于将指定B站直播间的弹幕等信息显示在直播画面上。

## 使用说明

1. OBS设置好Python目录（必须为python3.6版本）
2. clone本仓库
3. 添加本仓库的obs-danmu.py到OBS中。
4. 右侧的UI窗口如果如截图所示即可使用。
5. 填写房间号，在场景中添加一个文本，选择所添加的文本，点击确定即可。

## 截图

![image-20210303003729402](capture/cap)

## 未来考虑

目前只有显示弹幕的功能，属实有点鸡肋。

可以对接特定的游戏，比如英雄联盟，获取战绩等，然后进行相应的数据处理并显示。

## 声明

本代码仓库使用GPL许可证进行开源，请仔细阅读并遵守相关规则。

